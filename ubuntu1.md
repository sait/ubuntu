# Linea Base Servidor Linux Ubuntu


### Cambiarse a root
```
sudo -s
```

### Ajustes al teclado en Bash

#Si el editor vi falla al moverse
```
Crear el archivo
vi ~/.vimrc
	set nocompatible
	set backspace=2
```

#Si falla el PgUp/PgDn en el historial de comandos
```
Editar el archivo .inputrc
vi ~/.inputrc 
	# alternate mappings for "page up" and "page down" to search the history
	"\e[5~": history-search-backward
	"\e[6~": history-search-forward
	
El archivo /etc/inputrc al parecer es por sistema
```


### Actualizar password de root
```
passwd root
```

### Historial Ilimitado de Comandos
Con el objetivo de tener un historial de todos los comandos que se han ejecutado en nuestro servidor, configuraremos el shell para que mantenga el historial de comandos en forma ilimitada

El historial de comandos se guarda en el archivo oculto ~/.bash_history

```
vi ~/.bashrc

# COMMENT THIS LINES
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
#HISTSIZE=1000
#HISTFILESIZE=2000

# ADD THIS LINES TO THE END
# Historial ilimitado de comandos
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "

# MyFavoritePrompt
export PS1='\n\u@\H \w\n\$'

```



### Actualizar Sistema Operativo 
```
apt-get update        # Fetches the list of available updates
apt-get upgrade       # Strictly upgrades the current packages
apt-get dist-upgrade  # Installs updates (new ones)
```

### Instalar Programas Básicos
```
apt install htop
apt-get install mariadb-client-core-10.0
```

### Verificar Version de SO y CPU
```
cat /etc/*-release

cat /proc/version
	Linux version 4.4.0-89-generic (buildd@lgw01-18) (gcc version 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.4) ) #112-Ubuntu SMP Mon Jul 31 19:38:41 UTC 2017

cat /proc/cpuinfo
	processor       : 0
	vendor_id       : GenuineIntel
	cpu family      : 6
	model           : 62
	model name      : Intel(R) Xeon(R) CPU E5-2630L v2 @ 2.40GHz
	stepping        : 4
	cpu MHz         : 2399.998
	cache size      : 15360 KB
	fpu             : yes
	...

```

### Definir Zona horaria
```

	timedatectl set-timezone America/Phoenix


```

### Definir Nombre de Servidor HostName
```
vi /etc/hosts
vi /etc/hostname
shutdown now -r
```



### Definir DNS (Servidor para Nombres de Dominio)
El servidor para nombres de dominio es el que resuelve las direcciones IP cuando indicamos un nombre de dominio, normalmente el proveedor de hosting cuenta con un DNS pero no son muy confiables, se recomienda cambiarlos para utilizar los servidores DNS de Google editando el archivo /etc/resolv.conf

```
vi /etc/resolv.conf
	nameserver 8.8.8.8
	nameserver 8.8.4.4
```

PENDIENTE




### Configurar Adaptadores de red

Listar adaptadores
ls /sys/class/net/
	enp0s3  enp0s8  lo
	
vi /etc/network/interfaces
	# NAT Network start with dhcp
	auto enp0s3
	iface enp0s3 inet dhcp
	# OnlyHost network start static
	auto enp0s8
	iface enp0s8 inet static
	address 192.168.56.102
	network 192.168.56.0
	netmask 255.255.255.0
	brodcast 192.168.56.255

Reiniciar servicio de red
	service networking restart






### Mantener la Hora Exacta 
PENDIENTE


### Agregar llave de administrador para acceso automatico via SSH
cd ~/.ssh
vi authorized_keys
	aqui en este archivo, agregar tu llave publica de tu PC %USERPROFILE%/.ssh/id_rsa.pub
De no existir existe el folder ~/.ssh, hay que crear llave privada del servidor, usando
	ssh-keygen -t rsa 

### Reconfigurar SSH
Para evitar accesos de intrusos, cambiaremos el puerto de acceso, primero vamos a dejar el 22 y el puerto secreto, posteriormente solo dejaremos el puerto secreto.

```
#Agregar el puerto adicional para comuncacion
vi /etc/ssh/sshd_config
	Port 22
	Port 2222

	# Si queremos entrar como root desde la terminal ssh
	PermitRootLogin yes
	
service sshd restart

netstat -ntap
```
Hacer la prueba conectandonos mediante el puerto nuevo, si todo sale bien, borrar la linea de Port 22 y reiniciar servicio
Desde tu PC usar: ssh root@myserverip -p2222
En caso de falla puede ser necesario abrir el Firewall para el puerto

### Firewall de Ubuntu
```
Ver https://help.ubuntu.com/community/UFW

ufw enable

ufw status verbose

ufw allow 2222/tcp

ufw status verbose

ufw app list

```





### Servicios


Listar servicios
service --status-all



### Configurar Vi
PENDIENTE
El editor vi, lee las opciones defaul del archivo: ~/.exrc, la opcion set nowrap permite que las lineas largas, se extiendan a la derehca.

vi .exrc
	set nowrap


### Resetar servidor a las 2:00 am
Es recomendable hacer un reset del servidor diariamente.
Para limpiar memoria, reiniciar logs etc
crontab -e
	# Resetear servidor cada dia a las 2:00 am
	0 2 * * * /sbin/shutdown now -r
	
	
###Deshabilitar interfase grafica en el arranque
    vi /etc/default/grub
		#GRUB_CMDLINE_LINUX_DEFAULT=" quiet splash" 
		GRUB_CMDLINE_LINUX_DEFAULT="text"
    sudo update-grub
	shutdown now -r
    run startx if you need to load Gnome

	
### Quitar timeout al menu de arranque
PENDIENTE
vi /boot/grub/grub.conf
	timeout=0
	* quitar rhgb (Red Hat Graphical Boot)
shutdown now -r

### Instalar git
	PENDIENTE
	yum install git-core
	ssh-keygen
	git config --global user.name "Root"
	git config --global user.email root@localhost


### Instalar envio de correos

	Ver
		https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-as-a-send-only-smtp-server-on-ubuntu-16-04
		https://www.linode.com/docs/email/postfix/postfix-smtp-debian7

	apt install mailutils
	vi /etc/postfix/main.cf
		inet_interfaces = loopback-only
		inet_interfaces = loopback-only
		relayhost = [smtp.mailgun.org]:587
		smtp_sasl_password_maps = static:SMTPUSER:SMTPPASSWORD
		smtp_sasl_auth_enable = yes
		smtp_sasl_security_options = noanonymous
		smtp_use_tls = yes
		smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt

	service postfix restart
	
	echo "This is the body of the email" | mail -s "This is the subject line 10:23 ubuntu postfix mailgun" ignacio@sait.com.mx
	

	
### Instalar Nginx

	Ver https://www.digitalocean.com/community/tutorials/como-instalar-nginx-en-ubuntu-16-04-es
	  908  [2017-08-04 20:04:04] apt-get install nginx
	  909  [2017-08-04 20:05:20] ufw app list
	  910  [2017-08-04 20:05:48] ufw status verbose
	  911  [2017-08-04 20:07:28] ufw allow 'Nginx Full'
	  912  [2017-08-04 20:07:31] ufw status verbose
	  913  [2017-08-04 20:07:46] ufw status
	  914  [2017-08-04 20:08:22] systemctl status nginx
	  915  [2017-08-04 20:08:35] service nginx status
	  916  [2017-08-04 20:08:51] netstat -natp
	  917  [2017-08-04 20:09:06] cd /var/www
	  918  [2017-08-04 20:09:07] ll
	  919  [2017-08-04 20:09:08] cd html/
	  920  [2017-08-04 20:09:09] ll
	  921  [2017-08-04 20:09:12] vi index.nginx-debian.html

	Poner la definicion de los dominios en:
		/etc/nginx/conf.d/*.conf	( Usar terminacion .conf como le haciamos en Centos)
		/etc/nginx/sites-available	( Poner aqui todos los sitios que podrias usar, con cualquier terminacion)
        /etc/nginx/sites-enabled	( Poner aqui un link al archivo en sites-availables para los dominions que se activen)

### Instalar PHP

	https://www.vultr.com/docs/how-to-install-and-configure-php-70-or-php-71-on-ubuntu-16-04
	apt-get install php7.0 php7.0-fpm php7.0-cli php7.0-common php7.0-mbstring php7.0-gd php7.0-intl php7.0-mysql php7.0-mcrypt php7.0-zip php7.0-json php7.0-xml php7.0-xsl php7.0-soap php7.0-curl
	php -v
	Configurar PHP
	vi /etc/php/7.0/fpm/php.ini
		cgi.fix_pathinfo=0
		sendmail_path = /usr/sbin/sendmail -t

	

	Configurar Nginx para llamar a Php
	Se debe poner el archivo de configuracion del dominio
        location ~ \.php$ {
                root /var/www/html;
                try_files $uri =404;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                #fastcgi_pass 127.0.0.1:9000;
                fastcgi_pass unix:/run/php/php7.0-fpm.sock;
        }

Probar Mail en PHP
Probar PDO en Php



### Problemas para conectarse con Mysql
Al instalarse MariaDB creo el usuario root con password vacio 
select user,host,password,plugin from mysql.user
Si en plugin aparece unix_socket es porque esta usando el socket
Puedes crear otro usuario y password
O puedes quitar el plugin con update user set plugin=''
